# -*- coding: utf-8 -*-

from flask import Flask, jsonify, g, send_from_directory
from uuid import uuid4
from os.path import dirname, realpath, join, abspath
import time

__dir__ = abspath(dirname(realpath(__file__)))
img_dir = join(__dir__, 'img')

app = Flask(__name__)

pictures_list = [
    {
        'id': uuid4(),
        'src': '/img/beach-sunset-thailand.jpg',
        'tags': 'mer eau ciel bateau soleil',
    },
    {
        'id': uuid4(),
        'src': '/img/blue-eyed-cat-peeking.jpg',
        'tags': 'chat chaise table bureau meuble',
    },
    {
        'id': uuid4(),
        'src': '/img/business-cat-in-office.jpg',
        'tags': 'chat tasse café ordinateur table bureau meuble siège',
    },
    {
        'id': uuid4(),
        'src': '/img/camping-kettle-and-coffee-cup.jpg',
        'tags': 'nature camping forêt tasse café théière tente feuille',
    },
    {
        'id': uuid4(),
        'src': '/img/carrots-on-earth.jpg',
        'tags': 'carotte légume terre nature nourriture',
    },
    {
        'id': uuid4(),
        'src': '/img/coffee-beans.jpg',
        'tags': 'café grain bol',
    },
    {
        'id': uuid4(),
        'src': '/img/coffee-break.jpg',
        'tags': 'café grain cafetière cuillère table personne',
    },
    {
        'id': uuid4(),
        'src': '/img/coffee-mug-in-bean-pile.jpg',
        'tags': 'café grain tasse',
    },
    {
        'id': uuid4(),
        'src': '/img/flat-coffee-beans.jpg',
        'tags': 'café grain',
    },
    {
        'id': uuid4(),
        'src': '/img/friends-backpacking-together.jpg',
        'tags': 'terre mer eau personne voyage sac ciel côte nature',
    },
    {
        'id': uuid4(),
        'src': '/img/hiker-climbing-mountains.jpg',
        'tags': 'personne sac randonneur voyage nature arbre montagne ciel',
    },
    {
        'id': uuid4(),
        'src': '/img/indian-food-on-restaurant-table.jpg',
        'tags': 'nourriture table restaurant',
    },
    {
        'id': uuid4(),
        'src': '/img/kitten-peaking-through-greenery.jpg',
        'tags': 'chat feuille nature',
    },
    {
        'id': uuid4(),
        'src': '/img/palm-trees-sky.jpg',
        'tags': 'ciel arbre palmier nuage nature',
    },
    {
        'id': uuid4(),
        'src': '/img/plump-unripe-green-tomatoes-on-the-vine.jpg',
        'tags': 'tomate fruit légume nature feuille nourriture',
    },
    {
        'id': uuid4(),
        'src': '/img/seeds-on-a-strawberry.jpg',
        'tags': 'fraise feuille fruit nourriture nature',
    },
    {
        'id': uuid4(),
        'src': '/img/standing-in-woods.jpg',
        'tags': 'personne nature forêt terre feuille mousse arbre rocher mousse chaussure jeans',
    },
    {
        'id': uuid4(),
        'src': '/img/strawberries-in-hand.jpg',
        'tags': 'personne fruit fraise nature feuille',
    },
    {
        'id': uuid4(),
        'src': '/img/thai-food-takeout.jpg',
        'tags': 'nourriture bol table baguette restaurant cuillère soupe',
    },
    {
        'id': uuid4(),
        'src': '/img/waves-crashing-on-beach.jpg',
        'tags': 'mer sable eau vague écume plage',
    },
    {
        'id': uuid4(),
        'src': '/img/woman-pauses-at-foot-of-waterfall.jpg',
        'tags': 'personne chute eau nature rocher',
    },
]

@app.route('/api/pictures/')
def pictures():
    time.sleep(1)
    return jsonify(pictures_list)

@app.route('/img/<name>')
def img(name):
    return send_from_directory(img_dir, name)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=8888)
