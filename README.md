# Omnidoc React Exercise

Hello and welcome! This is a little exercise to confirm your React skills. We don't expect you to spend too much time on it (2 hours max is a good limit to aim for).

## Goal

Build a searchable image gallery as shown below: [![animated demo](doc/exo.gif)](doc/exo.mp4)

[Download video](doc/exo.mp4)

The idea is to assess that you can:
- clone a git repository
- run a Python script on your computer
- run a React project
- create a simple app with code, html and css
- structure your code nicely

We do not expect:
- advanced state management patterns (_e.g._ Redux)
- advanced API synchronization scheme
- code performance optimization
- automated tests

This should stay a small, quick project.

## Setup

1. Make sure you have git, Python (2.7+ or 3+) and virtualenv installed on your machine
2. Clone this git repository
3. Run the backend
4. Run the frontend

To run the backend, type these commands in a (bash) terminal, from the root directory of the git repository:

```shell
cd back
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python backend.py
```

To run the front-end, type these commands in an another terminal window, from the root directory of the git repository:

```shell
cd front
npm install
npm start
```

A "hello world" page should appear in your browser. The project was set up with `create-react-app`.

You're all set, you can start adding your code in the `front/src/` directory.


# Specs

Here is what the project should do. In case of doubt, refer to the gif above.

#### Load the resources from the API:

The backend API can be reached like this in Javascript:

```javascript
fetch('/api/pictures/').then(r => r.json().then(console.log))
```

This returns a JSON list of elements like this:

```json
[
  {
    "id": "b9846424-2d9f-4fe7-8255-634f9711f603",
    "src": "/img/beach-sunset-thailand.jpg",
    "tags": "mer ciel bateau soleil"
  },
  {
    "id": "5ae2625e-1b64-47fa-a243-fa141f302f76",
    "src": "/img/blue-eyed-cat-peeking.jpg",
    "tags": "chat chaise table bureau meuble"
  },
  ...
]
```

#### Display a "loading..." text while the resources are fetched

![](doc/screen_loading.png)

#### Display the following layout

A text input on top of the pictures. All pictures should be displayed with the same aspect ratio.

![](doc/screen_layout.jpg)


#### Accept search input and display only the matching pictures

The search is only client-side, there is no request to the API. Search is performed on the tags fields. To know if a picture should be displayed, the rule is: **each word searched must be equal or a substring of (at least) one tag**. A tag is a word in the tags field.

![](doc/screen_search.png)

Several words can be typed in:

![](doc/screen_search_2.png)

If no picture matches, display a simple text:

![](doc/screen_not_found.png)


#### That's it!

All done? Send us the `front/src/` folder (and optionally the package*.json files if they were modified).

Thank you for your time!
